# MQTT Thermometer
This repository brings you a simple thermometer based on Wemos D1 board using ESP8266 WIFI module and Arduino board support package. The temperature is read using one-wire bus and DS18B20 digital thermometer. It is possible to control a heater, simulated by and on-module blue LED. GUI is done thanks to off-line HTML and javascript and javascript MQTT library.

## MQTT Links
- MQTT online client - test transmission and reception: http://www.hivemq.com/demos/websocket-client/
- MQTT -> graph utility: https://tinker.yeoman.com.au/2015/05/11/simple-browser-based-graphical-display-of-mqtt-data/
- MQTT public broker: broker.mqttdashboard.com (port 8000 for websocket, 1883 for normal)

## Node-red MQTT client
- node-red @ raspberry pi: https://nodered.org/docs/hardware/raspberrypi
- Add "dashboard" using  "manager palette"
- temporary deployment: http://79.127.203.163:1880/#  and  http://79.127.203.163:1880/ui

## Arduino Links
- OLED display library: https://github.com/greiman/SSD1306Ascii
- MQTT library: https://github.com/knolleary/pubsubclient




## Other Links

- hosting: snackhost.eu
- node-red: https://www.digitalocean.com/community/tutorials/how-to-connect-your-internet-of-things-with-node-red-on-ubuntu-16-04
- mqtt + nodered: http://noderedguide.com/tag/mqtt/ and https://www.rs-online.com/designspark/building-distributed-node-red-applications-with-mqtt
- mqtt layer for node-red: https://flows.nodered.org/node/node-red-contrib-mqtt-broker
- mqtt websocket client - debug: http://www.hivemq.com/demos/websocket-client/
- public mqtt broker, port 1883, 8000: http://www.hivemq.com/try-out/
- open source mqtt broker - mosquitto: https://www.rs-online.com/designspark/building-distributed-node-red-applications-with-mqtt
- free node-red  - 24h: https://fred.sensetecnic.com/
- node-red : dashboard component: http://developers.sensetecnic.com/article/a-node-red-dashboard-using-node-red-dashboard/ and https://github.com/node-red/node-red-dashboard
- graphs: https://flows.nodered.org/node/node-red-contrib-graphs, IMPORTANT: before npm install, do: `cd ~/.node-red/`

## GUI and Hardware - pictures
![Hardware setup - SSD1306 0.96" OLED and Wemos D1](Pictures/setup_with_oled.jpg)
![Wemos D1 - used hardware platform](Pictures/wemos_d1.jpg)
![Offline Graphing Tool](Pictures/graphing_offline_tool.png)
![Node-Red - application](Pictures/node_red_application.png)
![Node-Red - dashboard](Pictures/node_red_dashboard.png)