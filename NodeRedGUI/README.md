# How to use
- Install node-red to your machine/server
- For raspberry pi, use this script:  
```
bash <(curl -sL https://raw.githubusercontent.com/node-red/raspbian-deb-package/master/resources/update-nodejs-and-nodered)
```
- Install "dashboard" from "Menu->Manage palette"
- Import Example.json using "Import->Clipboard" copy pasting its contents to the window.
- Navigate to: <your_server_ip:1880>/ui (e.g. 79.127.203.163:1880/ui) to see the GUI.