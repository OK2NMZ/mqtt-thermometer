# MQTT thermometer example

 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library and SSD1306 OLED display  

 It connects to an MQTT server then:  
 - publishes current temperature to the topic "temperature_in" every ten seconds
 - subscribes to the topic "heater_control", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
 - If the first character of the topic "heater_control" is an 1, switch ON the ESP Led,
    else switch it off  



 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.  


To install the ESP8266 board, (using Arduino 1.6.4+):
- Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
- Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
- Select your ESP8266 in "Tools -> Board"


# IMPORTANT:
- When you upload your firmware to Wemos D1 (Retired), please use a jumper wire to connect D8 to GND
- Czech tutorial: http://navody.arduino-shop.cz/navody-k-produktum/esp8266-vyvojova-deska-wemos-d1.html