/*
 MQTT thermometer example

 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library and SSD1306 OLED display

 It connects to an MQTT server then:
  - publishes current temperature to the topic "temperature_in" every ten seconds
  - subscribes to the topic "heater_control", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "heater_control" is an 1, switch ON the ESP Led,
    else switch it off

 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.

 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define I2C_ADDRESS 0x3C //OLED display address
#include <Wire.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiWire.h"

#include <OneWire.h>
#include <DallasTemperature.h>


// Update these with values suitable for your network.

const char* ssid = "AndroidAP";
const char* password = "koblihakobliha";
const char* mqtt_server = "185.8.239.191";

WiFiClient espClient;
PubSubClient client(espClient);
SSD1306AsciiWire oled;

// Instantiate OneWire library
OneWire oneWireDS(2); //GPIO2 - D9

// Instantiate DS18B20
DallasTemperature DS18B20(&oneWireDS);

long lastMsg = 0;
#define BUFFER_SIZE (64)
char msg[BUFFER_SIZE];
char txt[BUFFER_SIZE];
int value = 0;
int heater = 1;
float temperature;

void setup() 
{

  Wire.begin();                
  oled.begin(&Adafruit128x64, I2C_ADDRESS);
  oled.setFont(System5x7);
  oled.clear();
  oled.print("- MQTT thermometer -\n\n\n");
  oled.print("Connecting to WIFI...\n");
  
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  DS18B20.begin();
}


void redraw()
{
    oled.setFont(System5x7);
    oled.clear();
    oled.print("Temperature: \n\n");
    oled.setFont(CalLite24);
    sprintf(txt, "  %s 'C\n", msg);
    oled.print(txt);
    oled.setFont(System5x7);
    sprintf(txt, "Heater: %s", (heater)?"ON":"OFF");
    oled.print(txt);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) 
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) 
  {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if ((char)payload[0] == '1') 
  {
    heater = 1;
  }
  else
  {
    heater = 0;
  }

  redraw();
}

void reconnect() 
{
  // Loop until we're reconnected
  while (!client.connected()) 
  {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) 
    {
      Serial.println("connected");
      // ... and resubscribe
      client.subscribe("heater_control");
    } 
    else 
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


void loop() 
{

  if (!client.connected()) 
  {
    reconnect();
  }
  
  client.loop();

  long now = millis();
  if (now - lastMsg > 10000) 
  {
    lastMsg = now;

    DS18B20.requestTemperatures();
    temperature = DS18B20.getTempCByIndex(0);

    snprintf (msg, BUFFER_SIZE, "%.1f", temperature);
    Serial.print("Publish message: ");
    Serial.println(msg); //send to UART
    
    client.publish("temperature_in", msg); //send to MQTT broker
    
    redraw();
  }
}

